<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class Vin
 * @package app\models
 */
class Vin extends Model
{
    /**
     * @var
     */
    public $vin;

    /**
     * @var
     */
    private $vin_wmi_vds;

    /**
     * @var
     */
    private $vin_vis;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['vin'], 'required'],
            [['vin'], 'string', 'length' => [9, 17]],
        ];
    }

    /**
     * @return mixed
     */
/*    public function getVinStructure()
    {
        $headers = [];
        $items = [];

        $this->vin_wmi_vds = substr($this->vin,0,-6);
        $this->vin_vis = substr($this->vin,-6);

        $sql = <<<SQL
        SELECT
          *, CONCAT(VIN,SERIAL) VIN_CODE, CONCAT(MODELCODE,MODEL_OPT) MODELCODE_OPT
        FROM
          vindat
        JOIN 
          mdlcode
        ON 
          mdlcode.CATALOG = vindat.CATALOG AND mdlcode.CDNAME = vindat.CDNAME AND mdlcode.POSDATA = vindat.MDLPOS-1
        WHERE
          VIN=:vin_wmi_vds  AND SERIAL=:vin_vis;
SQL;

        $data =  Yii::$app->db2->createCommand($sql, [
            ':vin_wmi_vds' => $this->vin_wmi_vds,
            ':vin_vis' => $this->vin_vis,
        ])->queryAll();

        foreach ($data as $key1 => $model) {
            $sql2 = <<<SQL
            select
              cdindex.*,
              cdindex_jp_en.shashuko shashuko_jp_en,
              destcnt.shashucd,
              posname.*
            from
              cdindex
            left outer join cdindex_jp_en
              on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
            left outer join destcnt
              on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
            left outer join posname
              on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
            where
              cdindex.shashu =:shashu and cdindex.catalog =:catalog
SQL;
            $series = Yii::$app->db2->createCommand($sql2, [
                ':shashu' => $model['MODSERIES'],
                ':catalog' => $model['CATALOG'],
            ])->queryAll();


            foreach ($series as $key2 => $serie) {
                $headers[] = $serie['DATA21'];
                $headers[] = $serie['DATA22'];
                $headers[] = $serie['DATA23'];
                $headers[] = $serie['DATA24'];
                $headers[] = $serie['DATA25'];
                $headers[] = $serie['DATA26'];
                $headers[] = $serie['DATA27'];
                $headers[] = $serie['DATA28'];

                $items[$key1 . $key2] = [
                    'vehicle_id' => $serie['MDLDIR'] . '_' .$serie['NNO'] . '_' . $serie['CATALOG'],
                    'model_name' => $serie['SHASHUKO'],
                    'model_series' => $serie['SHASHU'],
                    'model_production_range' => $serie['FROM_DATE'] . '-' . $serie['UPTO_DATE'],
                    'catalog' => $serie['CATALOG'],
                ];

                if($serie['VARIATION1']) {
                    $items[$key1 . $key2][strtolower($serie['DATA21'])] = $serie['VARIATION1'];
                }
                if($serie['VARIATION2']) {
                    $items[$key1 . $key2][strtolower($serie['DATA22'])] = $serie['VARIATION2'];
                }
                if($serie['VARIATION3']) {
                    $items[$key1 . $key2][strtolower($serie['DATA23'])] = $serie['VARIATION3'];
                }
                if($serie['VARIATION4']) {
                    $items[$key1 . $key2][strtolower($serie['DATA24'])] = $serie['VARIATION4'];
                }
                if($serie['VARIATION5']) {
                    $items[$key1 . $key2][strtolower($serie['DATA25'])] = $serie['VARIATION5'];
                }
                if($serie['VARIATION6']) {
                    $items[$key1 . $key2][strtolower($serie['DATA26'])] = $serie['VARIATION6'];
                }
                if($serie['VARIATION7']) {
                    $items[$key1 . $key2][strtolower($serie['DATA27'])] = $serie['VARIATION7'];
                }
                if($serie['VARIATION8']) {
                    $items[$key1 . $key2][strtolower($serie['DATA28'])] = $serie['VARIATION8'];
                }
            }
        }

        $headers = array_diff(array_unique($headers), ['']);

        foreach ($headers as $header) {
            $header = strtolower($header);
            $models['header'][] = [
                $header => [
                    'fld' => $header,
                    'title' => $header,
                ]
            ];
        }

        $models['items'] = array_values($items);
        $models['cnt_items'] = count($items);
        $models['page'] = 1;

        return $models;
    }*/

    public function getVinStructure()
    {
        $headers = [];
        $items = [];
        $ser = [];

        $this->vin_wmi_vds = substr($this->vin,0,-6);
        $this->vin_vis = substr($this->vin,-6);

        $sql = <<<SQL
        SELECT
          *, CONCAT(VIN,SERIAL) VIN_CODE, CONCAT(MODELCODE,MODEL_OPT) MODELCODE_OPT
        FROM
          vindat
        JOIN 
          mdlcode
        ON 
          mdlcode.CATALOG = vindat.CATALOG AND mdlcode.CDNAME = vindat.CDNAME AND mdlcode.POSDATA = vindat.MDLPOS-1
        WHERE
          VIN=:vin_wmi_vds  AND SERIAL=:vin_vis;
SQL;

        $data = Yii::$app->db2->createCommand($sql, [
            ':vin_wmi_vds' => $this->vin_wmi_vds,
            ':vin_vis'     => $this->vin_vis,
        ])->queryAll();

        foreach ($data as $key1 => $model) {
            $vin_info[] = $model;
            $vin_info[$key1]['prod_yyyymm'] = date_unpack_ym($model['PRODYM']);

            $sql_spec_req = "
            SELECT 
              * 
            FROM 
              specvin 
            WHERE
             CATALOG = :catalog AND CDNAME = :cdname AND POSDATA = :pspecvin AND SPECREC <> ''";

            $spec_req = Yii::$app->db2->createCommand($sql_spec_req, [
                ':catalog'  => $model['CATALOG'],
                ':cdname'   => $model['CDNAME'],
                ':pspecvin' => --$model['PSPECVIN'],
            ])->queryOne();

            $vin_info[$key1]['SPECREC'] = $spec_req['SPECREC'];

            $sql2 = <<<SQL
            select
              cdindex.*,
              cdindex_jp_en.shashuko shashuko_jp_en
            from
              cdindex
            left outer join cdindex_jp_en
                  on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
            where
              cdindex.shashu =:shashu and cdindex.catalog =:catalog
SQL;
            $mod_name = Yii::$app->db2->createCommand($sql2, [
                ':shashu'  => $model['MODSERIES'],
                ':catalog' => $model['CATALOG'],
            ])->queryOne();

            $sql2 = <<<SQL
            select
              cdindex.*,
              cdindex_jp_en.shashuko shashuko_jp_en,
              destcnt.shashucd,
              posname.*,
              cdindex.FROM_DATE,
              cdindex.UPTO_DATE
            from
              cdindex
            left outer join cdindex_jp_en
                  on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
            left outer join destcnt
              on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
            left outer join posname
              on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
            where
              cdindex.shashu =:shashu and cdindex.catalog =:catalog and posname.NNO = :posname
SQL;
            $series = Yii::$app->db2->createCommand($sql2, [
                ':shashu'  => $model['MODSERIES'],
                ':catalog' => $model['CATALOG'],
                ':posname' => $model['POSNUM']
            ])->queryOne();


            if ($series) {
                $series['from_yyyymm'] = date_unpack_ym(@$series['FROM_DATE']);
                $series['upto_yyyymm'] = date_unpack_ym(@$series['UPTO_DATE']);


            //  Подбор подходящего атвомобиля(комплектации) по дате производства VIN
//            foreach ($series as $key2 => $serie) {
                $headers[] = $series['DATA21'];
                $headers[] = $series['DATA22'];
                $headers[] = $series['DATA23'];
                $headers[] = $series['DATA24'];
                $headers[] = $series['DATA25'];
                $headers[] = $series['DATA26'];
                $headers[] = $series['DATA27'];
                $headers[] = $series['DATA28'];

                $items[$key1] = [
                    'vehicle_id' => $series['MDLDIR'] . '_' .$model['POSNUM'] . '_' . $model['CATALOG'],
                    'model_name' => $mod_name['SHASHUKO'],
                    'model_series' => $model['MODSERIES'],
                    'catalog' => $model['CATALOG'],
                ];

                if ($series['FROM_DATE'] && $series['UPTO_DATE']) {
                    $items[$key1]['model_production_range'] = date_unpack_ym(@$series['FROM_DATE']) . '-' . date_unpack_ym(@$series['UPTO_DATE']);
                }

//
                if($series['VARIATION1']) {
                    $items[$key1][strtolower($series['DATA21'])] = $series['VARIATION1'];
                }
                if($series['VARIATION2']) {
                    $items[$key1][strtolower($series['DATA22'])] = $series['VARIATION2'];
                }
                if($series['VARIATION3']) {
                    $items[$key1][strtolower($series['DATA23'])] = $series['VARIATION3'];
                }
                if($series['VARIATION4']) {
                    $items[$key1][strtolower($series['DATA24'])] = $series['VARIATION4'];
                }
                if($series['VARIATION5']) {
                    $items[$key1][strtolower($series['DATA25'])] = $series['VARIATION5'];
                }
                if($series['VARIATION6']) {
                    $items[$key1][strtolower($series['DATA26'])] = $series['VARIATION6'];
                }
                if($series['VARIATION7']) {
                    $items[$key1][strtolower($series['DATA27'])] = $series['VARIATION7'];
                }
                if($series['VARIATION8']) {
                    $items[$key1][strtolower($series['DATA28'])] = $series['VARIATION8'];
                }
            }
        }

        $headers = array_diff(array_unique($headers), ['']);

        foreach ($headers as $header) {
            $header = strtolower($header);
            $models['header'][] = [
                $header => [
                    'fld'   => $header,
                    'title' => $header,
                ]
            ];
        }

        $models['items'] = array_values($items);
        $models['cnt_items'] = count($items);
        $models['page'] = 1;

        return $models;
    }

}
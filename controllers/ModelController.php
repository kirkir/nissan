<?php

namespace app\controllers;


use app\behaviors\QueryFilters;
use app\models\Models;
use yii\web\HttpException;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class ModelController extends Controller
{

    public function behaviors() {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'query_filters' => [
                'class' => QueryFilters::className(),
            ],
        ];
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionIndex()
    {
        $object = Yii::$app->request->get();

        $model = new Models();
        $model->model_id = 'm';
        $model->scenario = $model::SCENARIO_MODEL_INDEX;
        $model->setAttributes($object);
        $model->validate();

        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        return ['models' => $model->getModels()];
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionView()
    {
        $object = Yii::$app->request->get();

        $model = new Models();
        $model->setAttributes($object);
        $model->validate();

        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        return [
            'model' => $model->getModel()
        ];
    }
}
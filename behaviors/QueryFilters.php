<?php
namespace app\behaviors;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Class QueryFilters
 * @package app\behaviors
 */
class QueryFilters extends \yii\base\Behavior
{

    /**
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'getFilters'
        ];
    }

    /**
     * @throws HttpException
     */
    public function getFilters()
    {
        if (!Yii::$app->language = Yii::$app->request->get('lang'))
            Yii::$app->language = 'en';
            //throw new HttpException(400, 'параметр lang не задан');
    }
}
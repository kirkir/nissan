<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%locale}}`.
 */
class m200525_220606_create_locale_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%locale}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(5),
            'name' => $this->string(11),
            'default' => $this->boolean(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%locale}}');
    }
}

<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class Models
 * @package app\models
 */
class Models extends Model
{
    /**
     * @var
     */
    public $model_id;

    /**
     * @var
     */
    public $brand_id;

    /**
     * @var
     */
    public $catalog;
    /**
     * @var
     */
    public $lang;

    /**
     * @var array
     */
    public $params = [];

    /**
     * @var array
     */
    public $filter_iterate = [1, 2, 3, 4, 5, 6, 7, 8];

    /**
     * @var array
     */
    public static $grp_jp_desc = [
        'Z' => 'ENGINE,FUEL',
        'Y' => 'BODY,TRIM',
        'X' => 'CHASIS,POWER TRAIN,BRAKE',
        'W' => 'ELECTRICAL',
        'V' => 'ACCESSORY',
    ];

    public $zero_len = [
        1 => '00',
        2 => '0',
        3 => '',
    ];

    public $page = 1;

    public $page_size = 20;

    const SCENARIO_MODEL_INDEX = 'model_index';

    const SCENARIO_FILTER_INDEX = 'filter_index';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['brand_id', 'required', 'on' => self::SCENARIO_MODEL_INDEX],
            [['model_id'], 'required', 'except' => [self::SCENARIO_FILTER_INDEX]],
            [['page', 'page_size'], 'integer'],
            [['model_id', 'catalog', 'brand_id'], 'string'],
            ['params', 'each', 'rule' => ['integer'], 'message' => 'Свойство {attribute} должно быть массивом'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'model_id' => 'model_id',
            'params'   => 'params',
        ];
    }

    /**
     * @return mixed
     */
    public function getModels()
    {
        $catalog = ($this->brand_id == 'infinity') ? ' where cdindex.catalog in (\'CAINF\' ,\'ELINF\' ,\'ERINF\' ,\'USINF\')' : ' where cdindex.catalog not in (\'CAINF\' ,\'ELINF\' ,\'ERINF\' ,\'USINF\')';

        $sql = <<<SQL
        select
          cdindex.model_id as model_id,
          cdindex.shashuko as name, 
          lower(replace(replace(cdindex.shashuko, '/', '-'), ' ', '-')) as seo_url
--           cdindex.from_date as from_date,
--           cdindex.upto_date as upto_date
        from
          cdindex
        left outer join cdindex_jp_en 
          on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
        left outer join destcnt 
          on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu $catalog
        GROUP BY cdindex.SHASHUKO ORDER BY cdindex.shashuko
SQL;

        return \Yii::$app->db2->createCommand($sql)->queryAll();
    }

    /**
     * @return array
     */
    public function getModel()
    {
        $sql = <<<SQL
        select
          cdindex.*
        from
          cdindex
        left outer join cdindex_jp_en 
          on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
        left outer join destcnt 
          on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
        where
          cdindex.model_id = :model_id

SQL;
        $model = \Yii::$app->db2->createCommand($sql, [
            ':model_id' => $this->model_id,
        ])->queryOne();

        $brand_id = (in_array($model['CATALOG'], ['CAINF' ,'ELINF' ,'ERINF' ,'USINF'])) ? 'infinity' : 'nissan';

        return [
            'catalog_brand_id'   => $brand_id,
            'catalog_brand_name' => strtoupper($brand_id),
            'catalog_model_id'   => $model['MODEL_ID'],
            'catalog_model_name' => $model['SHASHUKO'],
        ];
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        $where_catalog = ($this->catalog) ? " and cdindex.CATALOG = '{$this->catalog}'" : (($this->brand_id == 'infinity') ? ' and cdindex.catalog in (\'CAINF\' ,\'ELINF\' ,\'ERINF\' ,\'USINF\')' : ' and cdindex.catalog not in (\'CAINF\' ,\'ELINF\' ,\'ERINF\' ,\'USINF\')');

        $sql = <<<SQL
        select
          cdindex.*,
          cdindex_jp_en.shashuko shashuko_jp_en, 
          destcnt.shashucd, 
          posname.*
        from
          cdindex
        left outer join cdindex_jp_en 
          on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
        left outer join destcnt 
          on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
        left outer join posname 
          on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
        where
          cdindex.model_id = :model_id $where_catalog
          
SQL;

        $models = \Yii::$app->db2->createCommand($sql, [
            ':model_id' => $this->model_id,
        ])->queryAll();

        $filters = [];
        foreach ($models as $key => $model) {
            foreach ($this->filter_iterate as $i => $value) {
                $data_slug = 'DATA2' . $value;
                $variation_slug = 'VARIATION' . $value;

                if (!empty($model[$data_slug])) {
                    $filters[] = [
                        'nic' => strtolower(str_replace(' ', '_', $model[$data_slug])),
                        'name' => $model[$data_slug],
                        'values' => $model[$variation_slug],
                    ];
                }
            }
        }
 
        $filter_map = [];
        foreach ($filters as $index => $elem) {

            ++$index;

            if (!isset($filter_map[$elem['nic']])) {
                $filter_map[$elem['nic']] = [
                    'filter_id' => $index,
                    'code' => $elem['nic'],
                    'name' => $elem['name'],
                ];
            }

            if ($elem['values']) {
                $filter_map[$elem['nic']]['values'][str_replace('/', '_' , strtolower($elem['values']))] = [
                    'filter_item_id' => $index,
                    'name' => $elem['values'],
                ];
//                $filter_map[$elem['nic']]['values'] = array_values($filter_map[$elem['nic']]['values']);

            }
        }

//        foreach ($filter_map as $key => $map) {
//            if ($this->params && !in_array($map['filter_id'], $this->params)) {
//                unset($filter_map[$key]);
//            }
//        }

        foreach ($filter_map as $key => $item) {
            $f_map[$key] = $item;
            $f_map[$key]['values'] = array_values($item['values']);
        }


        /** reindex @var  $item */
        $filter_map_r = [];
        $i = 0;

        if (@$f_map) {
            foreach ($f_map as $key => $item) {
                $filter_map_r[$i] = $item;
                if ($this->params) {
                    foreach ($this->params as $param) {
                        foreach ($filter_map_r[$i]['values'] as $value) {
                            if (@$value['filter_item_id'] == $param) {
                                $filter_map_r[$i]['values'] = [
                                    [
                                        'filter_item_id' => $value['filter_item_id'],
                                        'name' => $value['name'],
                                    ]
                                ];
                            }
                        }
                    }
                } else {
                    $filter_map_r[$i]['values'] = array_values($item['values']);
                }
                $i++;
            }
        }

        return $filter_map_r;
    }

    /**
     * @return array
     */
    public function getFilters2()
    {
        $sql = <<<SQL
        select
          cdindex.*,
          cdindex_jp_en.shashuko shashuko_jp_en, 
          destcnt.shashucd, 
          posname.*
        from
          cdindex
        left outer join cdindex_jp_en 
          on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
        left outer join destcnt 
          on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
        left outer join posname 
          on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
        where
          cdindex.model_id = :model_id
SQL;

        $models = \Yii::$app->db2->createCommand($sql, [
            ':model_id' => $this->model_id,
        ])->queryAll();

        $filters = [];
        foreach ($models as $key => $model) {
            foreach ($this->filter_iterate as $i => $value) {
                $data_slug = 'DATA2' . $value;
                $variation_slug = 'VARIATION' . $value;

                if (!empty($model[$data_slug])) {
                    $filters[] = [
                        'code' => strtolower(str_replace(' ', '/', $model[$data_slug])),
                        'name' => $model[$data_slug],
                        'values' => $model[$variation_slug],
                    ];
                }
            }
        }

        $filter_map = [];
        foreach ($filters as $index => $elem) {
            ++$index;

            if (!isset($filter_map[$elem['code']])) {
                $filter_map[$elem['code']] = [
                    'filter_id' => $index,
                    'code' => $elem['code'],
                    'name' => $elem['name'],
                ];
            }

            $filter_map[$elem['code']]['values'][str_replace('/', '_' , strtolower($elem['values']))] = [
                'id' => $index,
                'name' => $elem['values'],
            ];
        }

        return $filter_map;
    }

    public function getFiltersData($filter_collection)
    {
        $headers = [];
        $items = [];
        $variation = '';

        foreach ($filter_collection as $key => $filter_item) {
            $filter_row = explode('_', $key);
            $variation .= " and posname.variation".$filter_row['1']." in(" .   "'" . implode("', '", $filter_item) . "'" . ")";
        }

        $sql = <<<SQL
        select
          cdindex.*,
          cdindex_jp_en.shashuko shashuko_jp_en,
          destcnt.shashucd,
          posname.*
        from
          cdindex
        left outer join cdindex_jp_en
          on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
        left outer join destcnt
          on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
        left outer join posname
          on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
        where
          cdindex.model_id =:model_id $variation
SQL;

        $page = $this->page;
        $limit = $this->page_size;
        $from = ($page-1)*$limit;

        $count = Yii::$app->db2->createCommand($sql, [
            ':model_id' => $this->model_id,
        ])->queryAll();

        $series = Yii::$app->db2->createCommand($sql . ' LIMIT '.$from.','.$limit, [
            ':model_id' => $this->model_id,
        ])->queryAll();

        foreach ($series as $key2 => $serie) {

            $headers[] = $serie['DATA21'];
            $headers[] = $serie['DATA22'];
            $headers[] = $serie['DATA23'];
            $headers[] = $serie['DATA24'];
            $headers[] = $serie['DATA25'];
            $headers[] = $serie['DATA26'];
            $headers[] = $serie['DATA27'];
            $headers[] = $serie['DATA28'];

            $items[$serie['shashucd'] . $key2] = [
                'vehicle_id' => $serie['MDLDIR'] . '_' .$serie['NNO'] . '_' . $serie['CATALOG'],
                'model_name' => $serie['SHASHUKO'],
                'model_series' => $serie['SHASHU'],
                'model_production_range' => $serie['FROM_DATE'] . '-' . $serie['UPTO_DATE'],
                'catalog' => $serie['CATALOG'],
            ];

            if($serie['VARIATION1']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA21']))] = $serie['VARIATION1'];
            }
            if($serie['VARIATION2']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA22']))] = $serie['VARIATION2'];
            }
            if($serie['VARIATION3']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA23']))] = $serie['VARIATION3'];
            }
            if($serie['VARIATION4']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA24']))] = $serie['VARIATION4'];
            }
            if($serie['VARIATION5']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA25']))] = $serie['VARIATION5'];
            }
            if($serie['VARIATION6']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA26']))] = $serie['VARIATION6'];
            }
            if($serie['VARIATION7']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA27']))] = $serie['VARIATION7'];
            }
            if($serie['VARIATION8']) {
                $items[$serie['shashucd'] . $key2][strtolower(str_replace(' ', '_', $serie['DATA28']))] = $serie['VARIATION8'];
            }
        }

        $headers = array_diff(array_unique($headers), ['']);

        $models['header'][] = [
            'code' => 'model_name',
            'title' => 'model_name',
        ];

        foreach ($headers as $header) {
            $header = strtolower(str_replace(' ', '_', $header));
            $models['header'][] = [
                    'code' => $header,
                    'title' => $header,
            ];
        }

        $models['items'] = array_values($items);
        $models['cnt_items'] = count($count);
        $models['page'] = $page;

        return $models;
    }

    /**
     * @param $serie_attr
     * @return array
     */
    public function getModelInfo($serie_attr)
    {
        $sql = <<<SQL
            select
              cdindex.*,
              cdindex_jp_en.shashuko shashuko_jp_en,
              destcnt.shashucd,
              posname.*
            from
              cdindex
            left outer join cdindex_jp_en
              on cdindex_jp_en.shashu = cdindex.shashu and cdindex.catalog = 'jp'
            left outer join destcnt
              on destcnt.catalog = cdindex.catalog and destcnt.shashu = cdindex.shashu
            left outer join posname
              on destcnt.shashucd = posname.mdldir and destcnt.catalog = posname.catalog
            where
              posname.mdldir = :mdldir AND posname.nno = :nno AND posname.catalog = :catalog
SQL;

        $serie = Yii::$app->db2->createCommand($sql, [
            ':mdldir'  => $serie_attr[0],
            ':nno'     => $serie_attr[1],
            ':catalog' => $serie_attr[2],
        ])->queryOne();

        if ($serie) {
            $headers[] = ['model_name', $serie['SHASHUKO']];
            $headers[] = ['model_production_range', $serie['FROM_DATE'] . '-' . $serie['UPTO_DATE']];
            $headers[] = ['catalog', $serie['CATALOG']];

            $headers[] = [$serie['DATA21'], $serie['VARIATION1']];
            $headers[] = [$serie['DATA22'], $serie['VARIATION2']];
            $headers[] = [$serie['DATA23'], $serie['VARIATION3']];
            $headers[] = [$serie['DATA24'], $serie['VARIATION4']];
            $headers[] = [$serie['DATA25'], $serie['VARIATION5']];
            $headers[] = [$serie['DATA26'], $serie['VARIATION6']];
            $headers[] = [$serie['DATA27'], $serie['VARIATION7']];
            $headers[] = [$serie['DATA28'], $serie['VARIATION8']];

            $headers = array_filter($headers, function($v) {
                return array_filter($v) != array();
            });

            foreach ($headers as $header) {

                if ($header[0] == 'model_production_range') {
                    $date = explode('-', $header[1]);
                    $value = date_unpack_ym($date[0]) . '-' . date_unpack_ym($date[1]);
                } else {
                    $value = $header[1];
                }

                $attrubutes[] = [
                    'nic'   => strtolower($header[0]),
                    'name'  => strtolower($header[0]),
                    'value' => $value ?? null,
                ];
            }
        }

        return $attrubutes ?? [null];
    }

    /**
     * @param $serie_attr
     * @return array
     */
    public function getMgroups($serie_attr)
    {
        $locale = Locale::getLocale();

        if (@$serie_attr[2] == 'JP') {
            $sql = "
                select
                  DISTINCT SUBSTRING(PICGROUP,1,2) AS PICGROUP,
                  MDLDIR
                from
                  emoloc_jp
                where
                  emoloc_jp.mdldir = :mdldir AND emoloc_jp.catalog = :catalog
                ORDER BY PICGROUP";
        } else {
            $sql = "
                select
                  *
                from
                  genloc_all
                where
                  genloc_all.mdldir = :mdldir AND genloc_all.catalog = :catalog";
        }


        $mgroups = Yii::$app->db2->createCommand($sql, [
            ':mdldir'  => @$serie_attr[0],
            ':catalog' => @$serie_attr[2],
        ])->queryAll();

        foreach ($mgroups as $item) {

            if (@$serie_attr[2] == 'JP') {
                $sql = "
                select
                  *
                from
                  esecloc_jp
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog and esecloc_jp.picgroup = :picgroup";
            } else {
                $sql = "
                select
                  *
                from
                  gsecloc_all
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog and gsecloc_all.picgroup = :picgroup GROUP BY FIGURE ORDER BY FIGURE";
            }


            $childs = Yii::$app->db2->createCommand($sql, [
                ':mdldir'   => @$serie_attr[0],
                ':catalog'  => @$serie_attr[2],
                ':picgroup' => $item['PICGROUP'],
            ])->queryAll();

            $map_child = [];
            foreach ($childs as $child) {
                $map_child[] = [
                    'group_id' => $child['id'],
//                    'name' => (@$serie_attr[2] == 'JP') ? self::$grp_jp_desc[$child['PICGROUP']] : $child[$locale->pref],
                    'name' => (@$serie_attr[2] == 'JP') ? $child['PARTNAME'] : $child[$locale->pref],
                    'childs' => []
                ];
            }

            $mgroup[] = [
                'group_id' => $item['PICGROUP'],
                'name' => (@$serie_attr[2] == 'JP') ? self::$grp_jp_desc[str_replace(['1', '2', '3', '4'], '', $item['PICGROUP'])] : $item[$locale->pref],
                'childs' => $map_child,
            ];
        }

        $mgroups = [];
        foreach ($mgroup as $item) {
            if (@!$mgroups[$item['name']] == $item['name']) {
                $mgroups[$item['name']] = [
                    'group_id' => $item['group_id'],
                    'name' => $item['name'],
                    'childs' => $item['childs']
                ];
            } else {
                if ($mgroups[$item['name']]['childs']) {
                    $res = array_merge($mgroups[$item['name']]['childs'], $item['childs']);
                    $mgroups[$item['name']]['childs'] = $res;
                }
            }
        }

        return array_values($mgroups) ?? [null];
    }

    /**
     * @param $serie_attr
     * @return array
     */
    public function getSgroups($serie_attr)
    {
        $locale = Locale::getLocale();

        $group_id = Yii::$app->request->get('group_id');
        $codes = Yii::$app->request->get('codes');
        $node_ids = Yii::$app->request->get('node_ids');

        if ($group_id) {
            if (@$serie_attr[2] == 'JP') {
                $sql = "
                select
                  *
                from
                  esecloc_jp
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog and esecloc_jp.picgroup = :picgroup";
            } else {
                $sql = "
                select
                  *
                from
                  gsecloc_all
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog and gsecloc_all.picgroup = :picgroup GROUP BY FIGURE ORDER BY FIGURE";
            }
            $sgroups = Yii::$app->db2->createCommand($sql, [
                ':mdldir'   => @$serie_attr[0],
                ':catalog'  => @$serie_attr[2],
                ':picgroup' => $group_id,
            ])->queryAll();
        } else {
            if (@$serie_attr[2] == 'JP') {

                if ($codes) {
                    $code_values = "'" . implode("', '", $codes) . "'";
                    $query_prefix = "and esecloc_jp.picgroup IN ($code_values)";
                } else {
                    $node_ids_values = "'" . implode("', '", $node_ids) . "'";
                    $query_prefix = "and esecloc_jp.id IN ($node_ids_values)";
                }

                $sql = "
                select
                  *
                from
                  esecloc_jp
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog $query_prefix";
            } else {
                if ($codes) {
                    $code_values = "'" . implode("', '", $codes) . "'";
                    $query_prefix = "and gsecloc_all.picgroup IN ($code_values)";
                } else {
                    $node_ids_values = "'" . implode("', '", $node_ids) . "'";
                    $query_prefix = "and gsecloc_all.id IN ($node_ids_values)";
                }

                $sql = "
                select
                  *
                from
                  gsecloc_all
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog $query_prefix GROUP BY FIGURE ORDER BY FIGURE";
            }

            $sgroups = Yii::$app->db2->createCommand($sql, [
                ':mdldir'   => @$serie_attr[0],
                ':catalog'  => @$serie_attr[2],
            ])->queryAll();
        }

        foreach ($sgroups as $item) {
            $img_query = "select
                  *
                from
                  illnote
                where
                  mdldir = :mdldir AND catalog = :catalog and figure LIKE :figure";

            $img = Yii::$app->db2->createCommand($img_query, [
                ':mdldir'  => @$serie_attr[0],
                ':catalog' => @$serie_attr[2],
                ':figure'  => '%' . $item['FIGURE'] . '%',
            ])->queryOne();


            $sgroup[] = [
                'node_id'   => $item['id'],
                'name'      => (@$serie_attr[2] == 'JP') ? self::$grp_jp_desc[$item['PICGROUP']] : $item[$locale->pref],
                'image_id'  => $img['PIMGSTR'],
                'image_ext' => '.gif',
            ];
        }

        return $sgroup ?? [null];
    }

    public function getSgroups2($serie_attr)
    {
        $locale = Locale::getLocale();

        $group_id = Yii::$app->request->post('group_id');
        $codes = Yii::$app->request->post('codes');
        $node_ids = Yii::$app->request->post('node_ids');

        if ($group_id) {

            // and esecloc_jp.picgroup = :picgroup
            if (@$serie_attr[2] == 'JP') {
                $sql = "  
                select
                  *
                from
                  esecloc_jp 
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog and esecloc_jp.id = :picgroup";
            } else {
                $sql = "
                select
                  *
                from
                  gsecloc_all 
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog and gsecloc_all.id = :picgroup GROUP BY FIGURE ORDER BY FIGURE";
            }
            $sgroups = Yii::$app->db2->createCommand($sql, [
                ':mdldir'   => @$serie_attr[0],
                ':catalog'  => @$serie_attr[2],
                ':picgroup' => $group_id,
            ])->queryAll();
        } else {
            if (@$serie_attr[2] == 'JP') {

                if ($codes) {

                    $codes = Codes::find()->where(['in', 'code', $codes])->all();
                    $code_map = [];

                    foreach ($codes as $code) {
                        $code_arr = explode(',', $code->node_ids);
                        $code_map = array_merge($code_arr, $code_map);
                    }

                    $node_ids_values = "'" . implode("', '", $code_map) . "'";
                    $query_prefix = "and esecloc_jp.id IN ($node_ids_values)";
                } else {
                    $node_ids_values = "'" . implode("', '", $node_ids) . "'";
                    $query_prefix = "and esecloc_jp.id IN ($node_ids_values)";
                }

                $sql = "
                select
                  *
                from
                  esecloc_jp
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog $query_prefix";
            } else {
                if ($codes) {

                    $codes = Codes::find()->where(['in', 'code', $codes])->all();
                    $code_map = [];

                    foreach ($codes as $code) {
                        $code_arr = explode(',', $code->node_ids);
                        $code_map = array_merge($code_arr, $code_map);
                    }

                    $node_ids_values = "'" . implode("', '", $code_map) . "'";
                    $query_prefix = "and gsecloc_all.id IN ($node_ids_values)";

                } else {
                    $node_ids_values = "'" . implode("', '", $node_ids) . "'";
                    $query_prefix = "and gsecloc_all.id IN ($node_ids_values)";
                }

                $sql = "
                select
                  *
                from
                  gsecloc_all
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog $query_prefix GROUP BY FIGURE ORDER BY FIGURE";
            }

            $sgroups = Yii::$app->db2->createCommand($sql, [
                ':mdldir'   => @$serie_attr[0],
                ':catalog'  => @$serie_attr[2],
            ])->queryAll();
        }

        foreach ($sgroups as $item) {
            $img_query = "select
                  *
                from
                  illnote
                where
                  mdldir = :mdldir AND catalog = :catalog and figure LIKE :figure";

            $img = Yii::$app->db2->createCommand($img_query, [
                ':mdldir'  => @$serie_attr[0],
                ':catalog' => @$serie_attr[2],
                ':figure'  => '%' . $item['FIGURE'] . '%',
            ])->queryOne();

            $sgroup[] = [
                'node_id'   => $item['id'],
                'name'      => (@$serie_attr[2] == 'JP') ? $item['PARTNAME'] : $item[$locale->pref],
//                'name'      => (@$serie_attr[2] == 'JP') ? @self::$grp_jp_desc[$item['PICGROUP']] : $item[$locale->pref],
                'image_id'  => mb_strtoupper($img['PIMGSTR']) .  '_' . $serie_attr[2] . '_' . $serie_attr[0],
                'image_ext' => '.gif',
            ];
        }

        return $sgroup ?? [null];
    }

    /**
     * @param $serie_attr
     * @param $node_id
     * @return array
     */
    public function getParts($serie_attr, $node_id)
    {
        $locale = Locale::getLocale();

        if (@$serie_attr[2] == 'JP') {
            $sql = "
                select
                  *
                from
                  esecloc_jp
                where
                  esecloc_jp.mdldir = :mdldir AND esecloc_jp.catalog = :catalog and esecloc_jp.id = :id";
        } else {
            $sql = "
                select
                  *
                from
                  gsecloc_all
                where
                  gsecloc_all.mdldir = :mdldir AND gsecloc_all.catalog = :catalog and gsecloc_all.id = :id GROUP BY FIGURE ORDER BY FIGURE";
        }

        $sgroup = Yii::$app->db2->createCommand($sql, [
            ':mdldir'  => @$serie_attr[0],
            ':catalog' => @$serie_attr[2],
            ':id'      => $node_id,
        ])->queryOne();


        $img_query = "select
                  *
                from
                  illnote
                where
                  mdldir = :mdldir AND catalog = :catalog and figure LIKE :figure";

        $img = Yii::$app->db2->createCommand($img_query, [
            ':mdldir'  => @$serie_attr[0],
            ':catalog' => @$serie_attr[2],
            ':figure'  => '%' . $sgroup['FIGURE'] . '%',
        ])->queryOne();

        $sql2 = "
            select
              *
            from
              pcodenes
            where
              mdldir =:mdldir AND catalog =:catalog and figure =:figure GROUP BY partcode";

        $codenes = Yii::$app->db2->createCommand($sql2, [
            ':mdldir'  => intval($sgroup['MDLDIR']),
            ':catalog' => $sgroup['CATALOG'],
            ':figure'  => intval($sgroup['FIGURE']),
        ])->queryAll();

        $parts_data = [];

        foreach ($codenes as $codene) {
            $xC = $codene['LABEL_X'];
            $yC = $codene['LABEL_Y'];

            $sql3 = "
            SELECT 
              fastpppd.*,
              catalog.*, 
              catalog.SALES CATALOG_SALES,
              $xC as coordX,
              $yC as coordY
            FROM 
              catalog
            LEFT OUTER JOIN fastpppd_all as fastpppd
            ON fastpppd.CATALOG = catalog.CATALOG
            AND fastpppd.OEM1 = catalog.OEMCODE
            WHERE catalog.CATALOG =:catalog AND MDLDIR =:mdldir AND PARTCODE =:partcode
            GROUP BY fastpppd.id   
            ";

            $parts = Yii::$app->db2->createCommand($sql3, [
                ':mdldir'   => intval($sgroup['MDLDIR']),
                ':catalog'  => $sgroup['CATALOG'],
                ':partcode' => $codene['PARTCODE'],
            ])->queryAll();

            $parts_data = array_merge($parts_data, $parts);
        }

        foreach ($parts_data as $parts_datum) {

            $code_len = strlen($parts_datum['PARTCODE']);
            $chr_1_cnt = substr_count($parts_datum['PARTCODE'], '1');
            $chr_1_w = (int)(9 * $chr_1_cnt / 3);

            $image_id = mb_strtoupper($img['PIMGSTR']) . '_' . $serie_attr[2] . '_' . $this->zero_len[strlen($serie_attr[0])] . $serie_attr[0];
            $image_arr[] = $image_id;

            $parts_result[] = [
                'node_id'  => $parts_datum['id'],
                'name'     => $parts_datum['DESCRIPTION'],
                'number'   => $parts_datum['OEMCODE'],
                'hotspots' => [
                    [
                        'x1' => (int)($parts_datum['coordX'] / 2),
                        'y1' => (int)($parts_datum['coordY'] / 2),
                        'x2' => (int)($parts_datum['coordX']) + 9 * $code_len - $chr_1_w + 3,
                        'y2' => (int)($parts_datum['coordY']) + 14,
                        'image_id' => $image_id,
                        'hotspot_id' => $parts_datum['PARTCODE']
                    ]
                ],
            ];
        }

        foreach (array_unique($image_arr) as $item) {
            $images[] = [
                'image_id' => $item,
                'ext'      => '',
            ];
        }

        return [
            'node_id' => $node_id,
            'name'    => (@$serie_attr[2] == 'JP') ? self::$grp_jp_desc[$sgroup['PICGROUP']] : $sgroup[$locale->pref],
            'parts'   => $parts_result,
            'images'  => $images,
        ];
    }

    /**
     * @return array
     * todo for JP?
     */
    public function getCodesForTree()
    {
        $vehicle_id = Yii::$app->request->get('vehicle_id');
        $vehicle_id_arr = explode('_', $vehicle_id);

        $gseloc = Yii::$app->db2->createCommand("select * from gsecloc_all WHERE catalog  = :catalog and mdldir = :mdldir", [
            ':catalog' => $vehicle_id_arr[2],
            ':mdldir' => $vehicle_id_arr[0],
        ])->queryAll();

        $sql = '1<>1';
        foreach (ArrayHelper::getColumn($gseloc, 'id') as $item) {
            $sql .= ' or node_ids REGEXP ' . $item;
        }

        $codes = Codes::find()->where($sql)->all();

        return [
            'result' => ($codes) ? ArrayHelper::getColumn($codes, 'code') : null,
        ];
    }
}
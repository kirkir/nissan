<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locale".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $name
 * @property int|null $default
 */
class Locale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locale';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['default'], 'integer'],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 11],
            [['pref'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'pref' => 'Pref',
            'default' => 'Default',
        ];
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getLocale()
    {
        return (self::find()->where(['code' => Yii::$app->language])->one()) ?? self::find()->where(['default' => 1])->one();

    }
}

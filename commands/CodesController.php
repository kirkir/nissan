<?php

namespace app\commands;


use app\models\Codes;
use Yii;
use yii\console\Controller;

/**
 * Class CodesController
 * @package app\commands
 */
class CodesController extends Controller
{

    public function actionIndex()
    {
        $cache = Yii::$app->cache;
        $codes = $cache->get('data');

        if ($codes === false) {

            $gseloc = Yii::$app->db2->createCommand("select * from gsecloc_all", [])->queryAll();
            $codes = [];

            foreach ($gseloc as $item) {
                if (!isset($codes[$item['PARTNAME_E']])) {
                    $codes[$item['PARTNAME_E']] = [
                        'name'  => $item['PARTNAME_E'],
                        'nodes' => [$item['id']],
                    ];
                } else {
                    $codes[$item['PARTNAME_E']]['nodes'][] = $item['id'];
                }
            }

            $cache->set('data', $codes);
        }

        foreach ($codes as $code) {
            $concat = implode(',', $code['nodes']);

            if (!Codes::find()->where(['code' => md5($concat)])->one()) {
                $model = new Codes([
                    'code'     => md5($concat),
                    'name'     => $code['name'],
                    'node_ids' => $concat,
                ]);

                $model->save();
            }
        }

    }
}
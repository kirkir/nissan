<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%codes}}`.
 */
class m200630_131002_create_codes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%codes}}', [
            'id'       => $this->primaryKey(),
            'code'     => $this->string(200),
            'name'     => $this->string(200),
            'node_ids' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%codes}}');
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "codes".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $name
 * @property string|null $node_ids
 */
class Codes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'codes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['node_ids'], 'string'],
            [['code', 'name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'node_ids' => 'Node Ids',
        ];
    }
}

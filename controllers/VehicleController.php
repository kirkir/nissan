<?php

namespace app\controllers;


use app\behaviors\QueryFilters;
use app\models\Image;
use app\models\Models;
use app\models\Vin;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class VehicleController
 * @package app\controllers
 */
class VehicleController extends Controller
{

    public $params = [];
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'query_filters' => [
                'class' => QueryFilters::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sgroups2' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionVin()
    {
        $object = Yii::$app->request->get();

        $model = new Vin();
        $model->setAttributes($object);
        $model->validate();

        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        return $model->getVinStructure();
    }

    /**
     * @param $vehicle_id
     * @return array
     */
    public function actionGetById($vehicle_id)
    {
        $serie_attr = explode('_', $vehicle_id);
        $model = new Models();

        return $model->getModelInfo($serie_attr);
    }

    /**
     * @param $vehicle_id
     * @return array
     */
    public function actionMgroups($vehicle_id)
    {
        $serie_attr = explode('_', $vehicle_id);
        $model = new Models();

        return $model->getMgroups($serie_attr);
    }

    /**
     * @param $vehicle_id
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionSgroups($vehicle_id)
    {
        if (Yii::$app->request->get('group_id') || Yii::$app->request->get('codes') || Yii::$app->request->get('node_ids')) {

            $serie_attr = explode('_', $vehicle_id);
            $model = new Models();

            return $model->getSgroups($serie_attr);
        }

        throw new HttpException(560, 'параметры group_id или codes или node_ids обязательны для заполнения');
    }

    /**
     * @param $vehicle_id
     * @return array
     * @throws HttpException
     */
    public function actionSgroups2($vehicle_id)
    {
        if (Yii::$app->request->post('group_id') || Yii::$app->request->post('codes') || Yii::$app->request->post('node_ids')) {

            $serie_attr = explode('_', $vehicle_id);
            $model = new Models();

            return $model->getSgroups2($serie_attr);
        }

        throw new HttpException(560, 'параметры group_id или codes или node_ids обязательны для заполнения');
    }

    public function actionParts($vehicle_id, $node_id)
    {
        $serie_attr = explode('_', $vehicle_id);
        $model = new Models();

        return $model->getParts($serie_attr, $node_id);
    }

    public function actionCodesForTree()
    {
        if (Yii::$app->request->get('vehicle_id')) {
            $model = new Models();
            return $model->getCodesForTree();
        }

        throw new HttpException(560, Yii::t('app', 'отсутствует параметр vehicle_id'));
    }

    /**
     * @param $image_id
     * @return string
     * @throws HttpException
     */
    public function actionImage($image_id)
    {
        $object['image_id'] = $image_id;

        $model = new Image();
        $model->setAttributes($object);

        $model->validate();
        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        return $model->image;
    }
}
<?php

namespace app\models;

use ErrorException;
use yii\base\Model;
use yii\web\HttpException;

/**
 * Class Image
 * @property mixed image
 * @package app\models
 */
class Image extends Model
{

    public $zero_len = [
        1 => '00',
        2 => '0',
        3 => '',
    ];

    /**
     * @var
     */
    public $image_id;

    /**
     * @var
     */
    public $cdn_url = 'http://185.101.204.28:4489';

    /**
     * @var string
     */
    public $cdn_img_path = 'nissan_fast/imgs';

    /**
     * @var bool|string
     */
    public $local_img_path = '@app/web/images';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['image_id'], 'required'],
            [['image_id'], 'string'],

        ];
    }

    public function getImage()
    {
        $parse_image_id = explode('_', $this->image_id);
        $parse_image_id[2] = $this->zero_len[strlen($parse_image_id[2])] . $parse_image_id[2];

        $local_file = \Yii::getAlias($this->local_img_path) . '/' .  $parse_image_id[1] .'/'. $parse_image_id[2] . '/' . $parse_image_id[0] . '.gif';


        try {
            $cdn_image = file_get_contents($this->cdn_url . '/' . $this->cdn_img_path . '/'. $parse_image_id[1] .'/'. $parse_image_id[2] .'/SECIMG/' . $parse_image_id[0] . '.gif');
        } catch (ErrorException $e) {
            throw new HttpException(560, 'не удалось найти изображение на сервере ' . $this->cdn_url);
        }

        if (!file_exists($local_file)) {
            if (!is_dir(\Yii::getAlias($this->local_img_path) . '/' .  $parse_image_id[1] .'/'. $parse_image_id[2])) {
                mkdir(\Yii::getAlias($this->local_img_path) . '/' .  $parse_image_id[1] .'/'. $parse_image_id[2], 0777, true);
            }

            file_put_contents($local_file, $cdn_image);
        }

        header('Content-Type: image/jpeg');
        echo file_get_contents($local_file);
        exit;
    }

}
<?php

namespace app\controllers;


use app\behaviors\QueryFilters;
use app\models\Models;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class FilterController
 * @package app\controllers
 */
class FilterController extends Controller
{

    /**
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'query_filters' => [
                'class'  => QueryFilters::className(),
            ],
        ];
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionIndex()
    {
        $object = Yii::$app->request->get();
        $model = new Models();
        $model->scenario = $model::SCENARIO_MODEL_INDEX;


        foreach ($object as $name => $attribute) {
            if (!key_exists($name, $model->attributes))
                throw new HttpException(560, "не верный параметр $name");
        }

        $model->setAttributes($object);
        $model->validate();

        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        return ['filters' => $model->getFilters()];
    }

    /**
     * @return array|void
     * @throws HttpException
     */
    public function actionView()
    {
        $object = Yii::$app->request->get();
        $model = new Models();
        $model->scenario = $model::SCENARIO_MODEL_INDEX;


        foreach ($object as $name => $attribute) {
            if (!key_exists($name, $model->attributes))
                throw new HttpException(560, "не верный параметр $name");
        }

        $model->setAttributes($object);
        $model->validate();

        if ($model->errors) {
            throw new HttpException(560, current($model->firstErrors));
        }

        $filter_collection = [];

        foreach ($model->getFilters2() as $filter) {
            $filter_title = $filter['code'] . '_' . $filter['filter_id'];
            foreach ($filter['values'] as $key => $value) {
                if (in_array($value['id'], $model->params)) {
                    $filter_collection[$filter_title][] = $value['name'];
                }
            }
        }

        return $model->getFiltersData($filter_collection) ?? [null];

    }
}
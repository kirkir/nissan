<?php

function format_date($d){
    if (($d == '') or ($d == 0)) return 0;
    if ($d == 65535) return 999999; // '65535' = 0xFF - максимум в smallint

    $mm = substr($d, -2); // последние 2 - месяц
    $mm = str_pad($mm, 2, '0', STR_PAD_LEFT); // '1' = 200001

    $yy = substr($d, 0, -2); // год, без последних двух
    $yy_i = (int)$yy;
    $yyyy = '';
    if ($yy_i > 50){
        $yyyy = '19'.$yy;
    }
    else{
        $yyyy = '20'.str_pad($yy, 2, '0', STR_PAD_LEFT);
    }

    return (int)($yyyy.$mm);
}

function date_unpack_ym($d){
    if (($d == '') or ($d == 0)) return 0;
    if ($d == 65535) return 999999; // '65535' = 0xFF - максимум в smallint

    $mm = substr($d, -2); // последние 2 - месяц
    $mm = str_pad($mm, 2, '0', STR_PAD_LEFT); // '1' = 200001

    $yy = substr($d, 0, -2); // год, без последних двух
    $yy_i = (int)$yy;
    $yyyy = '';
    if ($yy_i > 50){
        $yyyy = '19'.$yy;
    }
    else{
        $yyyy = '20'.str_pad($yy, 2, '0', STR_PAD_LEFT);
    }

    return (int)($yyyy.$mm);
}